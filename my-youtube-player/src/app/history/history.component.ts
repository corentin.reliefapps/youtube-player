import { Component, OnInit } from '@angular/core';
import { VideoServiceService } from '../video-service.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {


  constructor(private videoService: VideoServiceService) { }

  ngOnInit() {this.videoService.displayHistoryAPI();
  }


  watch(url: string): void{
    this.videoService.toPlayer(url);
    this.videoService.addInHistoryAPI(url);
    console.log(`le string envoyé est : ${url}`)
  }

}
