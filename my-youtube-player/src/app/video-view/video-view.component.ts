import { Component, OnInit } from '@angular/core';
import { VideoServiceService } from '../video-service.service';
import { DomSanitizer} from '@angular/platform-browser'

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.css']
})
export class VideoViewComponent implements OnInit {

  playingUrl: string;

  constructor(private videoService : VideoServiceService) { }



  ngOnInit() {
  }

  favorite() {
    this.videoService.addToFavorite();
  }


}
