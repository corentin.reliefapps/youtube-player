import { Component, OnInit } from '@angular/core';
import { VideoServiceService } from '../video-service.service';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {

  constructor(private videoService: VideoServiceService) { }

  ngOnInit() { this.videoService.displayFavorite()
  }

  watch(url: string): void{
    this.videoService.toPlayer(url);
    this.videoService.addInHistory(url);
    console.log(`le string envoyé est : ${url}`)
  }
}
