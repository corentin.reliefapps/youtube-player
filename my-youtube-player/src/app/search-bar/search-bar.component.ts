import { Component, OnInit } from '@angular/core';
import { VideoViewComponent } from '../video-view/video-view.component'
import { HistoryComponent } from '../history/history.component'
import { VideoServiceService } from '../video-service.service'

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  constructor(private videoService: VideoServiceService) { }

  ngOnInit() {
  }

  search(url : string) :void{
    this.videoService.toPlayer(url);
    console.log(`le string envoyé est : ${url}`)
    this.videoService.addInHistoryAPI(url);
  }

}
