import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VideoServiceService {

  playingUrl: SafeResourceUrl;
  unsafePlayingUrl: string;
  historyArray: string[] = [];
  historyArrayAPI =
    {
      content: ''
    };
  historyArrayAPI2;
  favoriteArray: string[] = ["lien 1", "lien  2"];
  private apiUrl = "http://localhost:80/insert/history"
  private apiUrl2 = "http://localhost:80/historys";

  constructor(private sanitizer: DomSanitizer, private httpClient: HttpClient) { 
  }

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  addToFavorite(){
    this.favoriteArray = JSON.parse(window.localStorage.getItem("favorite"));
    if(this.favoriteArray == null){
      this.favoriteArray = [];
    }
    this.favoriteArray.push(this.unsafePlayingUrl);

    window.localStorage.setItem("favorite", JSON.stringify(this.favoriteArray));
  }

  displayFavorite(){
    this.favoriteArray = JSON.parse(window.localStorage.getItem("favorite"));
  }


  addInHistory(url: string):void{

    console.log(url);
    this.historyArray = JSON.parse(window.localStorage.getItem("history"));
    if(this.historyArray == null){
      this.historyArray = [];
    }
    this.historyArray.push(url);

    window.localStorage.setItem("history", JSON.stringify(this.historyArray));
  }

  addInHistoryAPI(url: string){
    this.historyArrayAPI = {content: url}
    this.httpClient
    .post(this.apiUrl, this.historyArrayAPI)
    .subscribe(
      () => {
        console.log('Enregistrement terminé !');
        this.displayHistoryAPI();
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );

  }

  displayHistoryAPI(){
    this.historyArray = []
    this.httpClient
    .get<any[]>(this.apiUrl2)
    .subscribe(
      (response) => {
        for(var k in response) {
          this.historyArray.push(response[k]["content"])
        }
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      }
    );
    console.log(this.historyArray)
  }

  toPlayer(url: string): void {
    this.unsafePlayingUrl = url;
    url = "https://www.youtube.com/embed/" + url.substring(32);
    this.playingUrl = this.transform(url);
    console.log(`videoservice playing url: ${this.playingUrl}`);

  }

}
